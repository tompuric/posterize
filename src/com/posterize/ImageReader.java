package com.posterize;

import java.awt.Color; 
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;

import javax.imageio.ImageIO;


public class ImageReader {

	public BufferedImage src = null;
	
	public void readImage(String filename) throws IOException
	{
		src = ImageIO.read(this.getClass().getResourceAsStream(filename));
	}
	public void readImage(String url, String filename) throws IOException
	{
		File f = new File(url + filename);
		src = ImageIO.read(f);
	}
	
	public void saveImage(String filename) throws IOException
	{
		ImageIO.write(src, "jpg", new File(filename));
	}
	
	public void saveImage(BufferedImage src, String filename) throws IOException
	{
		ImageIO.write(src, "jpg", new File(filename));
	}
	
	public void displayAllPixelRGB()
	{
		//There is method called getRGB() that extract the pixel value at (column, row)
		for(int column = 0; column < src.getWidth(); column++)
		{
			for(int row = 0; row < src.getHeight(); row++)
			{
				Color c = new Color(src.getRGB(column, row));
				System.out.println("Pixel value at (" + row + ", " + column + ") is:"
						+ " Alpha " + c.getAlpha()
						+ ", Red " + c.getRed()
						+ ", Green " + c.getGreen()
						+ ", Blue " + c.getBlue());
			}
		}
		
	}
	
	public BufferedImage modify(int posterizeValue)
	{
		BufferedImage img = new BufferedImage(src.getWidth(), src.getHeight(), BufferedImage.TYPE_INT_RGB);
		
		if (posterizeValue < 2) posterizeValue = 2;
		if (posterizeValue > 50) posterizeValue = 50;
		
		double numOfAreas = 256.0/posterizeValue;
		double numOfValues = 255.0/(posterizeValue - 1);
		
		for(int column = 0; column < src.getWidth(); column++)
		{
			for(int row = 0; row < src.getHeight(); row++)
			{
				int c = src.getRGB(column, row);
				int r, g, b;
				r = c >> 16 & 0x000000FF;
				g = c >> 8 & 0x000000FF;
				b = c & 0x000000FF;
				
				int newRed = colour(r, numOfAreas, numOfValues);
				int newGreen = colour(g, numOfAreas, numOfValues);
				int newBlue = colour(b, numOfAreas, numOfValues);
				
				img.setRGB(column, row, newRed << 16 | newGreen << 8 | newBlue);
			}
		}
		return img;
	}
	
	public int colour(int c, double numOfAreas, double numOfValues) {
		double AreaFloat = c/numOfAreas;
		int Area = (int)(AreaFloat);
		if (Area > AreaFloat) Area--;
		double newFloat = numOfValues*Area;
		int col = (int)(newFloat);
		if (col > newFloat) col--;
		return col;
	}

}
