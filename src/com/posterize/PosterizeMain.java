package com.posterize;

import java.awt.Dimension;
import java.awt.FileDialog;
import java.awt.FlowLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.image.BufferedImage;
import java.io.IOException;

import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JSlider;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;



public class PosterizeMain extends JFrame {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	public ImageReader ir = new ImageReader();
	public JFrame frame = null;
	public ImageIcon imageIcon = null;
	public JLabel imageLabel = null, sliderLabel;
	public JButton load, save;
	int value =50;

	public PosterizeMain() {
		
	}
	
	public void showWithButtonAndHandler() throws IOException {

		frame = new JFrame("Posterize My Image! By Tomislav Puric 312111967");
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.setResizable(true);
		frame.setVisible(true);
		frame.setMinimumSize(new Dimension(600, 600));
		

		frame.setLayout(new FlowLayout());

		ir.readImage("/coverAIPoster.png");
		imageIcon = new ImageIcon(ir.src);
		imageLabel = new JLabel(" ", imageIcon, JLabel.CENTER);
		frame.getContentPane().add(imageLabel);
		
		load = new JButton("Load Image");
		load.addActionListener(new MyButtonHandler());
		frame.add(load);
		
		save = new JButton("Save Image");
		save.addActionListener(new MyButtonHandler());
		frame.add(save);
		
		JSlider slider = new JSlider(2, 50, 50);
		slider.addChangeListener(new MySlideHandler());
		frame.add(slider);
		
		sliderLabel = new JLabel("Value: 50");
		frame.add(sliderLabel);
		
		frame.validate();
		frame.setSize(ir.src.getWidth(), ir.src.getHeight() + 100);
		frame.setLocationRelativeTo(null);
	}
	
	// inner class for slider event handling
	private class MySlideHandler implements ChangeListener {

		@Override
		public void stateChanged(ChangeEvent e) {
			value = ((JSlider) e.getSource()).getValue();
			sliderLabel.setText("Value = " + value);
			BufferedImage newImg = ir.modify(value);
			imageIcon = new ImageIcon(newImg);
			imageLabel.setIcon(imageIcon);
		}
		
	}
	
	// inner class for button event handling
	private class MyButtonHandler implements ActionListener {
		public void actionPerformed(ActionEvent e) {
			if (e.getActionCommand().equals(load.getText())) {
				// perform load operation
				try {
					loadImage();
				} catch (IOException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
			}
			else {
				try {
					saveImage();
				} catch (IOException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
			}
		}
		
	}
	public void saveImage() throws IOException {
		FileDialog fd = new FileDialog(this, "Posterize Image Saver", FileDialog.SAVE);
		fd.setVisible(true);
		fd.setLocationRelativeTo(null);
		String filename = fd.getFile();
		System.out.println(fd.getFile());
		System.out.println(fd.getDirectory());
		System.out.println(fd.getName());
		if (filename == null) return;
		ir.saveImage(ir.modify(value), fd.getDirectory() + fd.getFile());
	}
	
	public boolean loadImage() throws IOException {
		FileDialog fd = new FileDialog(this, "Posterize Image Locator", FileDialog.LOAD);
		fd.setVisible(true);
		fd.setLocationRelativeTo(null);
		if (fd.getFile() != null) {
			ir.readImage(fd.getDirectory(), fd.getFile());
			imageIcon = new ImageIcon(ir.src);
			imageLabel.setIcon(imageIcon);
			return true;
		}
		return false;
	}

	
	public static void main(String[] args) throws IOException {
		PosterizeMain f = new PosterizeMain();
		f.showWithButtonAndHandler();	
	}

}
